
  function infoboothchart(myChart) {
   
  
    var option = {
  
        tooltip: {
          trigger: 'axis',
          axisPointer: {
          type: 'cross',
          animation: true,
       label: {
       backgroundColor: '#505765'}}
        },
        grid: {
        bottom: 80
        },
        toolbox: {
        show: true,
        feature: {
        magicType: {
        show: true,
        title: {
            line: 'Line',
            bar: 'Bar',
        },
  
        type: ['line', 'bar',]
        },
        saveAsImage: {
        show: true,
        title: "Save Image"
      }
              }
        },
          legend: {
            x:'left',
            data:['Temp. PV', 'Hum. PV', 'Temp. SP', 'Hum. SP'],
            },
            dataZoom: [
              {
                  show: true,
                  realtime: true,
                  start: 75,
                  end: 100
              },
              {
                  type: 'inside',
                  realtime: true,
                  start: 25,
                  end: 85
              }
          ],
            xAxis: {
              type : 'category',
              data: date
            },
          yAxis: [
            {
              type : 'value',
              // scale:true,
              min:20,
              max: 50,
              // splitNumber: 5,
              boundaryGap: true,
              axisLabel : {
                formatter: '{value} °C'
            }
          },
          {
              type : 'value',
              // scale:true,
              min:20,
              max:70,
              // splitNumber: 5,
              boundaryGap: true,
              axisLabel : {
                formatter: '{value} %'
            }
             
          }
        ],
  
            series: [{
              name: 'Temp. PV',
              type: 'line',
              yAxisIndex: 0,
              data: inside_temp_array,
              markLine: {
                data: [
                    { name: 'Minimum', yAxis: 25, valueIndex:0},
                    { name: 'Maximum', yAxis: 33,  valueIndex:0}
                ],
                lineStyle:{color:"#c1222b",}
            },
              
          },
          {
              name: 'Hum. PV',
              type: 'line',
              yAxisIndex: 1,
              data: inside_hum_array,
              markLine: {
                data: [
                    { name: 'Minimum', yAxis: 45, valueIndex:1},
                    { name: 'Maximum', yAxis: 55, valueIndex:1}
                ],
                lineStyle:{color:"#27727a",}
            },
          },
          {
              name: 'Temp. SP',
              type: 'line',
              yAxisIndex: 0,
              data: ambient_temp_array
          },
          {
              name: 'Hum. SP',
              type: 'line',
              yAxisIndex: 1,
              data: ambient_hum_array
              
          }
        ]
        };
       
        setInterval(function (){
            axisData = (new Date()).toLocaleTimeString().replace(/^\D*/,'');
            var data0 = option.series[0].data;
            var data1 = option.series[1].data;
            var data2 = option.series[2].data;
            var data3 = option.series[3].data;
            $.ajax({
              "url": "/api/trends/AsuTrend/",
              "method": "POST",
              "data":{
                  'inside_temp':"TUD-101-Booth Inside Temp",
                  'inside_hum':"TUD-101-Booth Inside Humidity",
                  'ambient_temp':"TUD-102-Ambient Temperature",
                  'ambient_hum':"TUD-102-Ambient Humidity"
                },
                success:function(data) {
                  for(i=0;i<data['inside_temp'].length;i++){
                    data0.shift();
                    data0.push(data['inside_temp'][i]['data']);
                    option.xAxis.data.shift();
                    option.xAxis.data.push(data['inside_temp'][i]['created']);
                    data2.shift();
                    data2.push(30);
                    data3.shift();
                    data3.push(50);
                  }
                  
                  for(i=0;i<data['inside_hum'].length;i++){
                      data1.shift();
                      data1.push(data['inside_hum'][i]['data']);
                  }
                  
  
                }
              });
              // myChart.setOption(option);
              
              if (option && typeof option === "object") {
                myChart.hideLoading();
                myChart.setOption(option,  true), 
                $(function() {
                  function resize() {
                    setTimeout(function() {
                            myChart.resize()
                        }, 100)
                    }
                    
                    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
                });
                
              };
  
            }, 5000);
  }
  function infoovenchart(myChart1) {
    
    var option = {
  
      tooltip: {
        trigger: 'axis',
        axisPointer: {
        type: 'cross',
        animation: true,
     label: {
     backgroundColor: '#505765'}}
      },
      grid: {
      bottom: 80
      },
      toolbox: {
      show: true,
      feature: {
      magicType: {
      show: true,
      title: {
          line: 'Line',
          bar: 'Bar',
      },

      type: ['line', 'bar',]
      },
      saveAsImage: {
      show: true,
      title: "Save Image"
    }
            }
      },
        legend: {
          x:'left',
          data:['Temp. PV', 'Hum. PV', 'Temp. SP', 'Hum. SP'],
          },
          dataZoom: [
            {
                show: true,
                realtime: true,
                start: 75,
                end: 100
            },
            {
                type: 'inside',
                realtime: true,
                start: 25,
                end: 85
            }
        ],
          xAxis: {
            type : 'category',
            data: date
          },
        yAxis: [
          {
            type : 'value',
            // scale:true,
            min:20,
            max: 50,
            // splitNumber: 5,
            boundaryGap: true,
            axisLabel : {
              formatter: '{value} °C'
          }
        },
        {
            type : 'value',
            // scale:true,
            min:20,
            max:70,
            // splitNumber: 5,
            boundaryGap: true,
            axisLabel : {
              formatter: '{value} %'
          }
           
        }
      ],

          series: [{
            name: 'Temp. PV',
            type: 'line',
            yAxisIndex: 0,
            data: inside_temp_array,
            markLine: {
              data: [
                  { name: 'Minimum', yAxis: 25, valueIndex:0},
                  { name: 'Maximum', yAxis: 33,  valueIndex:0}
              ],
              lineStyle:{color:"#c1222b",}
          },
            
        },
        {
            name: 'Hum. PV',
            type: 'line',
            yAxisIndex: 1,
            data: inside_hum_array,
            markLine: {
              data: [
                  { name: 'Minimum', yAxis: 45, valueIndex:1},
                  { name: 'Maximum', yAxis: 55, valueIndex:1}
              ],
              lineStyle:{color:"#27727a",}
          },
        },
        {
            name: 'Temp. SP',
            type: 'line',
            yAxisIndex: 0,
            data: ambient_temp_array
        },
        {
            name: 'Hum. SP',
            type: 'line',
            yAxisIndex: 1,
            data: ambient_hum_array
            
        }
      ]
      };
        setInterval(function (){
          axisData = (new Date()).toLocaleTimeString().replace(/^\D*/,'');
          var data01 = option.series[0].data;
          var data11 = option.series[1].data;
          var data21 = option.series[2].data;
          var data31 = option.series[3].data;
          $.ajax({
            "url": "/api/trends/AsuTrend/",
            "method": "POST",
            "data":{
                'inside_temp':"TUD-101-Booth Inside Temp",
                'inside_hum':"TUD-101-Booth Inside Humidity",
                'ambient_temp':"TUD-102-Ambient Temperature",
                'ambient_hum':"TUD-102-Ambient Humidity"
              },
              success:function(data) {
                for(i=0;i<data['inside_temp'].length;i++){
                  data01.shift();
                  data01.push(data['inside_temp'][i]['data']);
                  option.xAxis.data.shift();
                  option.xAxis.data.push(data['inside_temp'][i]['created']);
                  data21.shift();
                  data21.push(30);
                  data31.shift();
                  data31.push(50);
                }
                
                for(i=0;i<data['inside_hum'].length;i++){
                    data11.shift();
                    data11.push(data['inside_hum'][i]['data']);
                }
                
              }
            });
              if (option && typeof option === "object") {
  
                myChart1.hideLoading();
                myChart1.setOption(option, true), $(function() {
                    function resize() {
                        setTimeout(function() {
                            myChart1.resize()
                        }, 100)
                    }
                    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
                });
              };
  
        }, 5000);
  
  }