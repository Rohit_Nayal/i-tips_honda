toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
}




// websocket scripts
// console.log(window.location);
var loc = window.location

var wsStart = 'ws://'
if (loc.protocol == 'https:') {
    wsStart = 'wss://'
};
var endpoint = wsStart + loc.host + loc.pathname



var socket = new WebSocket(endpoint)

socket.onmessage = function (e) {
    // console.log("messageeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee", e);
    // console.log(e.data);
    var chatDatamsg = JSON.parse(e.data)
    console.log(chatDatamsg.host);
    // console.log(chatDatamsg.firstname);


    toastr["warning"](chatDatamsg.firstname + " " + chatDatamsg.lastname + " Just Logged Into TSS via \n " + chatDatamsg.host + "\n & IP ADD: " + chatDatamsg.ip, "New Login")




}
socket.onopen = function (e) {
    // console.log("open", e);


}
socket.onerror = function (e) {
    // console.log("error", e);

}
socket.onclose = function (e) {
    // console.log("messageeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee", e);

}