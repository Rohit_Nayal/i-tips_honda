from django.shortcuts import render

# Create your views here.
def point1(request):
    context = {}
    point = requests.get('https://10.116.112.70:39320/iotgateway/read?ids=HONDANVH.TIPS.M798', verify=False)
    objects = (json.loads(point.text))
    data = objects['readResults'][0]['v']
    print("***************************************************************************",data)
    # camera = Cameras.objects.get(id=1)
    response = ""
    if data == True:
        path= settings.MEDIA_ROOT
        detector= ObjectDetector(loadPath="/home/hondapc1/Desktop/ivision-tips/SVMs/rightp1.1.svm")

        # vs = cv2.VideoCapture(camera.ip_address)
        # vs = cv2.VideoCapture("rtsp://admin:vijender1124@10.116.112.89:554/profile0")
        vs = cv2.VideoCapture("rtsp://admin:taikisha@2019@10.116.112.94:554/profile0")
        path = settings.MEDIA_ROOT
        # while True:
        ret, frame = vs.read()
        string = str(random.randint(0, 500) )
        image = cv2.imwrite(path+"/Original_image/"+string+".jpg", frame)
        image = cv2.imread(path+"/Original_image/"+string+".jpg")
        detected = detector.detect(image, annotate="POINT 1 DETECTED")
        print(detected)
        if detected == "POINT DETECTED":
            response = True
            request.session['point1'] = True
        else:
            response = False    
    return JsonResponse(response, safe=False)



def point2(request):
    context = {}
    point = requests.get('https://10.116.112.70:39320/iotgateway/read?ids=HONDANVH.TIPS.M798', verify=False)
    objects = (json.loads(point.text))
    data = objects['readResults'][0]['v']
    # camera = Cameras.objects.get(id=1)
    response = ""
    if data == True:
        path= settings.MEDIA_ROOT
        detector= ObjectDetector(loadPath="/home/hondapc1/Desktop/ivision-tips/SVMs/p2right_1feb.svm")
        # vs = cv2.VideoCapture(camera.ip_address)
        # vs = cv2.VideoCapture("rtsp://admin:vijender1124@10.116.112.89:554/profile0")
        vs = cv2.VideoCapture("rtsp://admin:taikisha@2019@10.116.112.94:554/profile0")
        
        path = settings.MEDIA_ROOT
        # while True:
        ret, frame = vs.read()
        string = str(random.randint(0, 500) )
        image = cv2.imwrite(path+"/Original_image/"+string+".jpg", frame)
        image = cv2.imread(path+"/Original_image/"+string+".jpg")
        detected = detector.detect(image, annotate="POINT 2 DETECTED")
        print(detected)
        
        if detected == "POINT DETECTED":
            response = True
            request.session['point2'] = True
        else:
            response = False    
    return JsonResponse(response, safe=False)
 


def point3(request):
    
    context = {}
    point = requests.get('https://10.116.112.70:39320/iotgateway/read?ids=HONDANVH.TIPS.M798', verify=False)
    objects = (json.loads(point.text))
    data = objects['readResults'][0]['v']
    # print(data)
    response = ""
    # camera = Cameras.objects.get(id=1)
    if data == True:
        path= settings.MEDIA_ROOT
        detector= ObjectDetector(loadPath="/home/hondapc1/Desktop/ivision-tips/SVMs/p3right_1feb.svm")
        # vs = cv2.VideoCapture(camera.ip_address)
        # vs = cv2.VideoCapture("rtsp://admin:vijender1124@10.116.112.89:554/profile0")
        vs = cv2.VideoCapture("rtsp://admin:taikisha@2019@10.116.112.94:554/profile0")
        
        path = settings.MEDIA_ROOT
        # while True:
        ret, frame = vs.read()
        string = str(random.randint(0, 500) )
        image = cv2.imwrite(path+"/Original_image/"+string+".jpg", frame)
        image = cv2.imread(path+"/Original_image/"+string+".jpg")
        detected = detector.detect(image, annotate="POINT 3 DETECTED")
        print(detected)
        
        if detected == "POINT DETECTED":
            response = True
            request.session['point3'] = True
        else:
            response = False    
    return JsonResponse(response, safe=False)


def point4(request):
    
    context = {}
    point = requests.get('https://10.116.112.70:39320/iotgateway/read?ids=HONDANVH.TIPS.X049', verify=False)
    objects = (json.loads(point.text))
    data = objects['readResults'][0]['v']
    return JsonResponse(data, safe=False)
