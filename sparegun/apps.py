from django.apps import AppConfig


class SparegunConfig(AppConfig):
    name = 'sparegun'
