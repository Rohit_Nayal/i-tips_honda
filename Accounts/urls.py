from django.conf.urls import url
from django.contrib.auth.views import LoginView
from Accounts import views

app_name = "Accounts"
# handler404 = views.handler404
# handler500 = views.handler500
urlpatterns = [
    url(r'^$', views.Welcome,name='welcome'),
    url(r'^login/$', LoginView.as_view(template_name='login.html'), name="login"),
    url(r'^index/$', views.Dashboard,name='index'),
    # url(r'^profile/(?P<username>\w+)/$', views.user_profile,name='profile'),
    url(r'^logout/$', views.user_logout,name='logout'),
    # url(r'^register/$', views.register,name='register'),
    url(r'^newuser/$', views.newuser, name='newuser'),
    url(r'^thank/$', views.thank, name='thank'),
    # url(r'^user_data/$', views.UserData.as_view(), name='Userdata'),
    
    
]