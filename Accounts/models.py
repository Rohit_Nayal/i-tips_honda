from django.db import models
from django.contrib.auth.models import User


# Create your models here.

class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    email = models.EmailField(default='', max_length=254, blank=True, null=True)
    phone = models.CharField(max_length=11,default='', blank=True, null=True)

    # city = models.CharField(max_length=100,default='', blank=True, null=True)
    department = models.CharField(max_length=100,default='', blank=True, null=True)

    def __str__(self):
        if self.user.email:
            return self.user.email
        else:
            return "Some user"