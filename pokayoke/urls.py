from django.conf.urls import url
from django.contrib.auth.views import LoginView
from pokayoke import views

app_name = "pokayoke"

urlpatterns = [
    url('right_side/', views.rightside, name='load_Rightpage'),
    url('point1/', views.point1, name='Rpoint1'),
    url('point2/', views.point2, name='Rpoint2'),
    url('point3/', views.point3, name='Rpoint3'),
    url('point4/', views.point4, name='point4'),
]
