from django.apps import AppConfig


class PokayokeConfig(AppConfig):
    name = 'pokayoke'
