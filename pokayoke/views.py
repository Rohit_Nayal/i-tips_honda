from django.shortcuts import render
import argparse, requests
import json, cv2, random
from django.http import JsonResponse
from django.conf import settings
from Detector.detector import ObjectDetector

# threading, cv2, os, random, glob, errno, json, base64

# Create your views here.
def rightside(request):
    context  = {}
    request.session["point1"] = False
    # request.session["point2"] = False
    # request.session["point3"] = False
    context['point1_triggered'] = request.session["point1"]
    # context['point2_triggered'] = request.session["point2"]
    # context['point3_triggered'] = request.session["point3"]
    return render(request, "pokayoke/right_side.html", context)



def point1(request):
    context = {}
    point = requests.get('https://10.116.112.70:39320/iotgateway/read?ids=HONDANVH.TIPS.M796', verify=False)
    objects = (json.loads(point.text))
    data = objects['readResults'][0]['v']
    response = ""
    if data == True:
        path = settings.MEDIA_ROOT
        detector= ObjectDetector(loadPath="/home/i-tips/Desktop/i-tips_honda/SVMs/rightp1.1.svm")
        vs = cv2.VideoCapture("rtsp://admin:vijender1124@10.116.112.89:554/profile0")
        ret, frame = vs.read()
        string = str(random.randint(0, 500) )
        image = cv2.imwrite(path+"/Original_image/"+string+".jpg", frame)
        image = cv2.imread(path+"/Original_image/"+string+".jpg")
        detected = detector.detect(image, annotate="POINT 1 DETECTED")
        if detected == "POINT DETECTED":
            response = True
            request.session['point1'] = True
        else:
            response = False    
    return JsonResponse(response, safe=False)



def point2(request):
    context = {}
    point = requests.get('https://10.116.112.70:39320/iotgateway/read?ids=HONDANVH.TIPS.M796', verify=False)
    objects = (json.loads(point.text))
    data = objects['readResults'][0]['v']
    response = ""
    if data == True:
        path= settings.MEDIA_ROOT
        detector= ObjectDetector(loadPath="/home/i-tips/Desktop/i-tips_honda/SVMs/p2right_1feb.svm")
        vs = cv2.VideoCapture("rtsp://admin:vijender1124@10.116.112.89:554/profile0")
        ret, frame = vs.read()
        string = str(random.randint(0, 500) )
        image = cv2.imwrite(path+"/Original_image/"+string+".jpg", frame)
        image = cv2.imread(path+"/Original_image/"+string+".jpg")
        detected = detector.detect(image, annotate="POINT 2 DETECTED")
        if detected == "POINT DETECTED":
            response = True
            request.session['point2'] = True
        else:
            response = False    
    return JsonResponse(response, safe=False)


def point3(request):
    context = {}
    point = requests.get('https://10.116.112.70:39320/iotgateway/read?ids=HONDANVH.TIPS.M796', verify=False)
    objects = (json.loads(point.text))
    data = objects['readResults'][0]['v']
    response = ""
    if data == True:
        path= settings.MEDIA_ROOT
        detector= ObjectDetector(loadPath="/home/i-tips/Desktop/i-tips_honda/SVMs/p3right_1feb.svm")
        vs = cv2.VideoCapture("rtsp://admin:vijender1124@10.116.112.89:554/profile0")
        ret, frame = vs.read()
        string = str(random.randint(0, 500) )
        image = cv2.imwrite(path+"/Original_image/"+string+".jpg", frame)
        image = cv2.imread(path+"/Original_image/"+string+".jpg")
        detected = detector.detect(image, annotate="POINT 3 DETECTED")
        
        if detected == "POINT DETECTED":
            response = True
            request.session['point3'] = True
        else:
            response = False    
    return JsonResponse(response, safe=False) 


def point4(request):
    
    context = {}
    point = requests.get('https://10.116.112.70:39320/iotgateway/read?ids=HONDANVH.TIPS.X049', verify=False)
    objects = (json.loads(point.text))
    data = objects['readResults'][0]['v']
    return JsonResponse(data, safe=False)      