import dlib
import cv2
import numpy as np
import os, random
from Detector.models import camera_objects
from django.conf import settings
# from playsound import playsound


font = cv2.FONT_HERSHEY_SIMPLEX


class ObjectDetector(object):
    def __init__(self,options=None,loadPath=None):
        #create detector options
        self.options = options
        if self.options is None:
            self.options = dlib.simple_object_detector_training_options()

        #load the trained detector (for testing)
        if loadPath is not None:
            self._detector = dlib.simple_object_detector(loadPath)

    def _prepare_annotations(self,annotations):
        annots = []
        for (x,y,xb,yb) in annotations:
            annots.append([dlib.rectangle(left=int(x),top=int(y),right=int(xb),bottom=int(yb))])
        return annots

    def _prepare_images(self,imagePaths):
        images = []
        for imPath in imagePaths:
            image = cv2.imread(imPath)
            image = cv2.cvtColor(image,cv2.COLOR_BGR2RGB)
            images.append(image)
            
        return images

    def fit(self, imagePaths, annotations, visualize=False, savePath=None):
        annotations = self._prepare_annotations(annotations)
        images = self._prepare_images(imagePaths)
        self._detector = dlib.train_simple_object_detector(images, annotations, self.options)
        #visualize HOG
        if visualize:
            win = dlib.image_window()
            win.set_image(self._detector)
            dlib.hit_enter_to_continue()

        #save detector to disk
        if savePath is not None:
            self._detector.save(savePath)

        return self

    def predict(self,image):
        boxes = self._detector(image)
        preds = []
        for box in boxes:
            (x,y,xb,yb) = [box.left(),box.top(),box.right(),box.bottom()]
            preds.append((x,y,xb,yb))
        return preds
    def detect(self, image, annotate=None):
        image =  cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        preds = self.predict(image)
        if preds:
            # for (x,y,xb,yb) in preds:
            #     image = cv2.cvtColor(image,cv2.COLOR_RGB2BGR)
            #     #draw and annotate on image
            #     cv2.rectangle(image,(x,y),(xb,yb),(0,0,255),2)
            #     if annotate_text is not None and type(annotate_text)==str:
            #         cv2.putText(image,annotate_text,(x+5,y-5),cv2.FONT_HERSHEY_SIMPLEX,1.0,(0,0,255),2)

            return "POINT DETECTED"    
        else:
            return "ERROR"



    def detect_zone2(self,image,camera_id, annotate=None):
        image = cv2.cvtColor(image,cv2.COLOR_BGR2RGB)
        print(image)
        preds = self.predict(image)
        check_points = camera_objects.objects.filter(camera_id=camera_id).order_by('id')
        predictions = {}
        counter = 0
        sub_counter = 0
        x_error=100
        y_error= 100
        for i, check_point in enumerate(check_points):
            check_point

            path = settings.MEDIA_ROOT
            self._detector = dlib.simple_object_detector(path+"/"+str(check_point.svms))
            predictions['zone'+str(camera_id)+'_part'+str(i+1)] = self.predict(image)
            if predictions['zone'+str(camera_id)+'_part'+str(i+1)]:
                annotate_text ='zone'+str(camera_id)+'_part'+str(i+1)
                for (x,y,xb,yb) in predictions['zone'+str(camera_id)+'_part'+str(i+1)]:
                    image = cv2.cvtColor(image,cv2.COLOR_RGB2BGR)
                    #draw and annotate on image
                    cv2.rectangle(image,(x,y),(xb,yb),(0,0,255),2)
                    if annotate_text is not None and type(annotate_text)==str:
                        cv2.putText(image,annotate_text,(x+5,y-5),cv2.FONT_HERSHEY_SIMPLEX,1.0,(0,0,255),2)
            else:
                y_error += 110
                cv2.putText(image,'Error AT zone'+str(camera_id)+'_part'+str(i+1),(x_error, y_error), font, 4,(0,0,255),2,cv2.LINE_AA)          

            # self._detector = dlib.simple_object_detector("/home/nagesh/Desktop/ivision_taikisha/SVMs/ZONE2_part3.svm")
            # preds_part3_zone2 = self.predict(image)

            # self._detector = dlib.simple_object_detector("/home/nagesh/Desktop/ivision_taikisha/SVMs/ZONE2_part4.svm")
            # preds_part4_zone2 = self.predict(image)
           
            # test = bool([a for a in predictions.items() if len(a) != 0])
            # if not test:
            # for key, value in predictions.items():
            #     # if all(value )
                
            #     if value is None or len(value)==0:
            #         # sub_counter += 1
            #         # if counter == sub_counter:
            #         #     cv2.putText(image,'Detecting Wrong Object',(x_error, y_error), font, 4,(255,0,0),2,cv2.LINE_AA)
            #         # else:
                    
            #     else:
            #         annotate_text =key
            #         for (x,y,xb,yb) in value:
            #             image = cv2.cvtColor(image,cv2.COLOR_RGB2BGR)
            #             #draw and annotate on image
            #             cv2.rectangle(image,(x,y),(xb,yb),(0,0,255),2)
            #             if annotate_text is not None and type(annotate_text)==str:
            #                 cv2.putText(image,annotate_text,(x+5,y-5),cv2.FONT_HERSHEY_SIMPLEX,1.0,(0,0,255),2)    
        # else:
        #     cv2.putText(image,'Processiong Wrong Object',(x_error, y_error), font, 4,(255,0,0),2,cv2.LINE_AA)
        # if preds and preds_part2_zone2 and preds_part3_zone2 and preds_part4_zone2:
        #     annotate ="PART_1_ok"
        #     for (x,y,xb,yb) in preds:
        #         image = cv2.cvtColor(image,cv2.COLOR_RGB2BGR)
        #         #draw and annotate on image
        #         cv2.rectangle(image,(x,y),(xb,yb),(0,0,255),2)
        #         if annotate is not None and type(annotate)==str:
        #             cv2.putText(image,annotate,(x+5,y-5),cv2.FONT_HERSHEY_SIMPLEX,1.0,(0,0,255),2)

        #     annotate_text ="PART_2_ok"
        #     for (x,y,xb,yb) in preds_part2_zone2:
        #         image = cv2.cvtColor(image,cv2.COLOR_RGB2BGR)
        #         #draw and annotate on image
        #         cv2.rectangle(image,(x,y),(xb,yb),(0,0,255),2)
        #         if annotate_text is not None and type(annotate_text)==str:
        #             cv2.putText(image,annotate_text,(x+5,y-5),cv2.FONT_HERSHEY_SIMPLEX,1.0,(0,0,255),2)
            
            
            


        #     annotate_text_3 ="PART_3_ok"
        #     for (x,y,xb,yb) in preds_part3_zone2:
        #         image = cv2.cvtColor(image,cv2.COLOR_RGB2BGR)
        #         #draw and annotate on image
        #         cv2.rectangle(image,(x,y),(xb,yb),(0,0,255),2)
        #         if annotate_text_3 is not None and type(annotate_text_3)==str:
        #             cv2.putText(image,annotate_text_3,(x+5,y-5),cv2.FONT_HERSHEY_SIMPLEX,1.0,(0,0,255),2)


        #     annotate_text_3 ="PART_4_ok"
        #     for (x,y,xb,yb) in preds_part4_zone2:
        #         image = cv2.cvtColor(image,cv2.COLOR_RGB2BGR)
        #         #draw and annotate on image
        #         cv2.rectangle(image,(x,y),(xb,yb),(0,0,255),2)
        #         if annotate_text_3 is not None and type(annotate_text_3)==str:
        #             cv2.putText(image,annotate_text_3,(x+5,y-5),cv2.FONT_HERSHEY_SIMPLEX,1.0,(0,0,255),2)        


        #     cv2.putText(image,"This ZONE is OK",(100, 100),cv2.FONT_HERSHEY_SIMPLEX,1.0,(0,0,255),2)
            
        # if preds or preds_part2_zone2 or preds_part3_zone2 or preds_part4_zone2:
        #     if preds :
        #         annotate ="PART_1_ok"
        #         for (x,y,xb,yb) in preds:
        #             image = cv2.cvtColor(image,cv2.COLOR_RGB2BGR)
        #             #draw and annotate on image
        #             cv2.rectangle(image,(x,y),(xb,yb),(0,0,255),2)
        #             if annotate is not None and type(annotate)==str:
        #                 cv2.putText(image,annotate,(x+5,y-5),cv2.FONT_HERSHEY_SIMPLEX,1.0,(0,0,255),2)
        #     else:            
        #         # detector = ObjectDetector(loadPath="upper_nut.svm")
        #         cv2.putText(image,'Error point 1 missing',(835, 350), font, 4,(0,0,255),2,cv2.LINE_AA) 
            
            
        #     if preds_part2_zone2 :
        #         annotate_text ="PART_2_ok"
        #         for (x,y,xb,yb) in preds_part2_zone2:
        #             image = cv2.cvtColor(image,cv2.COLOR_RGB2BGR)
        #             #draw and annotate on image
        #             cv2.rectangle(image,(x,y),(xb,yb),(0,0,255),2)
        #             if annotate_text is not None and type(annotate_text)==str:
        #                 cv2.putText(image,annotate_text,(x+5,y-5),cv2.FONT_HERSHEY_SIMPLEX,1.0,(0,0,255),2)
        #     else:            
        #         # detector = ObjectDetector(loadPath="upper_nut.svm")
        #         cv2.putText(image,'Error point 2 missing',(835, 350), font, 4,(0,0,255),2,cv2.LINE_AA) 
           
            
        #     if preds_part3_zone2 :
        #         annotate_text ="PART_3_ok"
        #         for (x,y,xb,yb) in preds_part3_zone2:
        #             image = cv2.cvtColor(image,cv2.COLOR_RGB2BGR)
        #             #draw and annotate on image
        #             cv2.rectangle(image,(x,y),(xb,yb),(0,0,255),2)
        #             if annotate_text is not None and type(annotate_text)==str:
        #                 cv2.putText(image,annotate_text,(x+5,y-5),cv2.FONT_HERSHEY_SIMPLEX,1.0,(0,0,255),2)
        #     else:            
        #         # detector = ObjectDetector(loadPath="upper_nut.svm")
        #         cv2.putText(image,'Error AT point 3',(400, 250), font, 4,(0,0,255),2,cv2.LINE_AA)


        #     if preds_part4_zone2 :
        #         annotate_text ="PART_4_ok"
        #         for (x,y,xb,yb) in preds_part4_zone2:
        #             image = cv2.cvtColor(image,cv2.COLOR_RGB2BGR)
        #             #draw and annotate on image
        #             cv2.rectangle(image,(x,y),(xb,yb),(0,0,255),2)
        #             if annotate_text is not None and type(annotate_text)==str:
        #                 cv2.putText(image,annotate_text,(x+5,y-5),cv2.FONT_HERSHEY_SIMPLEX,1.0,(0,0,255),2)
        #     else:            
        #         # detector = ObjectDetector(loadPath="upper_nut.svm")
        #         cv2.putText(image,'Error AT point 4',(500, 110), font, 4,(0,0,255),2,cv2.LINE_AA)    


            

        # else:            
        #     cv2.putText(image,'Error Wron Zone/ Object',(835, 350), font, 4,(0,0,255),2,cv2.LINE_AA) 
                
        cv2.waitKey(30000)
        return image






    