from django.contrib import admin
from Detector.models import Cameras, detection, camera_objects

# Register your models here.



class CamerasAdmin(admin.ModelAdmin):
    list_display = ('title', 'ip_address', 'slug')
    ordering = ('title',)
 
admin.site.register(Cameras, CamerasAdmin)
admin.site.register(detection)
admin.site.register(camera_objects)
