from django.shortcuts import render
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from Detector.detector import ObjectDetector
from django.http import StreamingHttpResponse
from imutils.video import VideoStream, FPS
from PIL import Image
from django.core.files.storage import FileSystemStorage
from io import BytesIO    
import numpy as np
import threading, cv2, os, random, glob, errno, json, base64
from django.template import loader, Context
import argparse, requests

from imutils.paths import list_images
from ROI.box_selector import BoxSelector
from scripts.gatther_annotaions import create_annotation

from Detector.models import Cameras, detection, camera_objects
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)



def detect_position(SVM_path, campera_ip, point_num):
    point = requests.get('https://10.116.112.70:39320/iotgateway/read?ids=HONDANVH.TIPS.M797', verify=False)
    objects = (json.loads(point.text))
    data = objects['readResults'][0]['v']
    response = False
    if data == True:
        path= settings.MEDIA_ROOT
        detector= ObjectDetector(loadPath=SVM_path)
        vs = cv2.VideoCapture(campera_ip)
        ret, frame = vs.read()
        string = str(random.randint(0, 500) )
        image = cv2.imwrite(path+"/Original_image/"+string+".jpg", frame)
        image = cv2.imread(path+"/Original_image/"+string+".jpg")
        detected = detector.detect(image, annotate=point_num)
        if detected == "POINT DETECTED":
            response = True
            # request.session['point1_left'] = True
            # data = {'res':response,
            #         'point1_left_session': request.session['point1_left']  }
        
    return response


def detect_position_right(SVM_path, campera_ip, point_num):
    point = requests.get('https://10.116.112.70:39320/iotgateway/read?ids=HONDANVH.TIPS.M796', verify=False)
    objects = (json.loads(point.text))
    data = objects['readResults'][0]['v']
    response = False
    if data == True:
        path= settings.MEDIA_ROOT
        detector= ObjectDetector(loadPath=SVM_path)
        vs = cv2.VideoCapture(campera_ip)
        ret, frame = vs.read()
        string = str(random.randint(0, 500) )
        image = cv2.imwrite(path+"/Original_image/"+string+".jpg", frame)
        image = cv2.imread(path+"/Original_image/"+string+".jpg")
        detected = detector.detect(image, annotate=point_num)
        if detected == "POINT DETECTED":
            response = True
            # request.session['point1_left'] = True
            # data = {'res':response,
            #         'point1_left_session': request.session['point1_left']  }
        
    return response        

def detect_position_rightsp(SVM_path, campera_ip, point_num):
    point = requests.get('https://10.116.112.70:39320/iotgateway/read?ids=HONDANVH.TIPS.M798', verify=False)
    objects = (json.loads(point.text))
    data = objects['readResults'][0]['v']
    response = False
    if data == True:
        path= settings.MEDIA_ROOT
        detector= ObjectDetector(loadPath=SVM_path)
        vs = cv2.VideoCapture(campera_ip)
        ret, frame = vs.read()
        string = str(random.randint(0, 500) )
        image = cv2.imwrite(path+"/Original_image/"+string+".jpg", frame)
        image = cv2.imread(path+"/Original_image/"+string+".jpg")
        detected = detector.detect(image, annotate=point_num)
        if detected == "POINT DETECTED":
            response = True
            # request.session['point1_left'] = True
            # data = {'res':response,
            #         'point1_left_session': request.session['point1_left']  }
        
    return response


def grab_image(path=None, stream=None, url=None):

    if path is not None:
        image =cv2.imread(path)

    else:

        if url is not None:
            resp = urlib.urlopen(url)
            data = resp.read()


        elif stream is not None:
            data= stream.read

        image =np.asarray(bytearray(data), dtype ="uint8")
        image = cv2.imdecode(image, cv2.IMREAD_COLOR)

    return image                         



def index(request):
    context  = {}
    context['zones'] = Cameras.objects.all().order_by('id')
    return render(request, "index.html", context)
    # return render(request, "rightside.html", context)



def rightside(request):
    context  = {}
    request.session["point1"] = False
    request.session["point2"] = False
    request.session["point3"] = False
    context['point1_triggered'] = request.session["point1"]
    context['point2_triggered'] = request.session["point2"]
    context['point3_triggered'] = request.session["point3"]
    return render(request, "rightside.html", context)


def leftside(request):
    context  = {}
    request.session["point1_left"] = False
    request.session["point2_left"] = False
    request.session["point3_left"] = False
    context['point1_triggered'] = request.session["point1_left"]
    context['point2_triggered'] = request.session["point2_left"]
    context['point3_triggered'] = request.session["point3_left"]
    return render(request, "leftside.html", context)        

def spare(request):
    context  = {}
    request.session["point1_left"] = False
    request.session["point2_left"] = False
    request.session["point3_left"] = False
    context['point1_triggered'] = request.session["point1_left"]
    context['point2_triggered'] = request.session["point2_left"]
    context['point3_triggered'] = request.session["point3_left"]
    return render(request, "spare_gun_right.html", context) 

def Lpoint1(request):
    context = {}
    response=""
    svm_path = "SVMs/POINT1_leftside.svm"
    cam_ip = "rtsp://admin:vijender@1124@10.116.112.91:554/profile0" 
    point_num = "POINT 1 DETECTED"
    print("session", request.session['point1_left'])
    if request.session['point1_left'] == True:
        print("truiessssssssssssssss")
        point = requests.get('https://10.116.112.70:39320/iotgateway/read?ids=HONDANVH.TIPS.M3411', verify=False)
        objects = (json.loads(point.text))
        data = objects['readResults'][0]['v']

        if data == True:
            print("if case ")
            # pass
        else:
            print("else case ")
            response = detect_position(svm_path, cam_ip, point_num)

    else:   
        print('Faslseeeeeeeeeeeeeeeeeeeeeeeeeeeeeee') 
        point = requests.get('https://10.116.112.70:39320/iotgateway/read?ids=HONDANVH.TIPS.M797', verify=False)
        objects = (json.loads(point.text))
        data = objects['readResults'][0]['v']
        response = ""
        if data == True:
            
            path= settings.MEDIA_ROOT
            detector= ObjectDetector(loadPath=svm_path)
            vs = cv2.VideoCapture(cam_ip)
            ret, frame = vs.read()
            string = str(random.randint(0, 500) )
            image = cv2.imwrite(path+"/Original_image/"+string+".jpg", frame)
            image = cv2.imread(path+"/Original_image/"+string+".jpg")
            detected = detector.detect(image, annotate="POINT 1 DETECTED")
            print(detected)
            if detected == "POINT DETECTED":
                response = True
                request.session['point1_left'] = True
                
            else:
                response = False
                    
    return JsonResponse(response, safe=False)

def Lpoint2(request):
    context = {}
    response=""
    svm_path = "SVMs/POINT2_left_multi.svm"
    cam_ip = "rtsp://admin:vijender@1124@10.116.112.91:554/profile0" 
    point_num = "POINT 2 DETECTED"
    print("session", request.session['point2_left'])
    if request.session['point2_left'] == True:
        print("truiessssssssssssssss")
        point = requests.get('https://10.116.112.70:39320/iotgateway/read?ids=HONDANVH.TIPS.M3412', verify=False)
        objects = (json.loads(point.text))
        data = objects['readResults'][0]['v']

        if data == True:
            print("if case ")
            # pass
        else:
            print("else case ")
            response = detect_position(svm_path, cam_ip, point_num)

    else:   
        print('Faslseeeeeeeeeeeeeeeeeeeeeeeeeeeeeee') 
        point = requests.get('https://10.116.112.70:39320/iotgateway/read?ids=HONDANVH.TIPS.M797', verify=False)
        objects = (json.loads(point.text))
        data = objects['readResults'][0]['v']
        response = ""
        if data == True:
            
            path= settings.MEDIA_ROOT
            detector= ObjectDetector(loadPath=svm_path)
            vs = cv2.VideoCapture(cam_ip)
            ret, frame = vs.read()
            string = str(random.randint(0, 500) )
            image = cv2.imwrite(path+"/Original_image/"+string+".jpg", frame)
            image = cv2.imread(path+"/Original_image/"+string+".jpg")
            detected = detector.detect(image, annotate="POINT 2 DETECTED")
            print(detected)
            if detected == "POINT DETECTED":
                response = True
                request.session['point2_left'] = True
                
            else:
                response = False
                
    return JsonResponse(response, safe=False)


def Lpoint3(request):
    context = {}
    response=""
    svm_path = "SVMs/POINT3_left_side_multi.svm"
    cam_ip = "rtsp://admin:vijender@1124@10.116.112.91:554/profile0" 
    point_num = "POINT 3 DETECTED"
    print("session", request.session['point3_left'])
    if request.session['point3_left'] == True:
        print("truiessssssssssssssss")
        point = requests.get('https://10.116.112.70:39320/iotgateway/read?ids=HONDANVH.TIPS.M3413', verify=False)
        objects = (json.loads(point.text))
        data = objects['readResults'][0]['v']

        if data == True:
            print("if case ")
            # pass
        else:
            print("else case ")
            response = detect_position(svm_path, cam_ip, point_num)

    else:   
        print('Faslseeeeeeeeeeeeeeeeeeeeeeeeeeeeeee') 
        point = requests.get('https://10.116.112.70:39320/iotgateway/read?ids=HONDANVH.TIPS.M797', verify=False)
        objects = (json.loads(point.text))
        data = objects['readResults'][0]['v']
        response = ""
        if data == True:
            
            path= settings.MEDIA_ROOT
            detector= ObjectDetector(loadPath=svm_path)
            vs = cv2.VideoCapture(cam_ip)
            ret, frame = vs.read()
            string = str(random.randint(0, 500) )
            image = cv2.imwrite(path+"/Original_image/"+string+".jpg", frame)
            image = cv2.imread(path+"/Original_image/"+string+".jpg")
            detected = detector.detect(image, annotate="POINT 3 DETECTED")
            print(detected)
            if detected == "POINT DETECTED":
                response = True
                request.session['point3_left'] = True
                
            else:
                response = False
                
    return JsonResponse(response, safe=False)


def Lpoint4(request):
    
    context = {}
    point = requests.get('https://10.116.112.70:39320/iotgateway/read?ids=HONDANVH.TIPS.X04A', verify=False)
    objects = (json.loads(point.text))
    data = objects['readResults'][0]['v']
    return JsonResponse(data, safe=False)




def get_cam(request):
    context = {}
    context["cameras"] = Cameras.objects.all().order_by("id")
    return render(request, 'getdata.html', context)

def gather_annotation(request):
    context = {}
    if request.method == "POST":
        response_a = create_annotation(request.POST.get('annotaion_npy'), request.POST.get('image_npy'), request.POST.get('dataset'))
        return render(request, 'train.html', context)


    path = settings.MEDIA_ROOT
    context['img_list'] =[]
    context['first_img'] = list()
    for entry in os.listdir(path):
        fullPath = os.path.join(path, entry)
        if os.path.isdir(fullPath):
            context['img_list'].append(entry)
            context['first_img'] = context['first_img'] + list(fullPath)
        else:
            context['first_img'].append(fullPath)
    return render(request, 'annotate.html', context)


def train(request):
    context={}
    path = "Annotations/"
    context['annotations'] = list()
    for entry in os.listdir(path):
        context['annotations'].append(entry)
    path = "Image_matrix/"
    context['image_matrix'] = list()
    for entry in os.listdir(path):
        context['image_matrix'].append(entry)
    
    if request.POST:
        annots = np.load("Annotations/"+request.POST.get('annotate_npy'))
        imagePaths = np.load("Image_matrix/"+request.POST.get('img_npy'))
        detector = ObjectDetector()
        svmPath = "SVMs/"+ request.POST.get('create_svm')
        context['response'] = detector.fit(imagePaths,annots,visualize=True,savePath=svmPath)
        print(context['response'])
        
    return render(request, 'train.html', context)

def capture(request, slug):
    context = {}
    context['cameras'] =Cameras.objects.all()
    camera = Cameras.objects.get(slug=slug)
    request.session['part'] = ""
    request.session['camera_url'] =""
    if request.POST:
        request.session['camera_url'] = "/detect/capture/"+camera.slug+"/"
        request.session['part'] = request.POST.get("Part_name")
        context["current_cam"] = request.session['camera_url']
        context["current_part"] = request.session['part']
        media_dir = settings.MEDIA_ROOT
        new_dir_path = os.path.join(media_dir, request.POST.get("Part_name"))
        try:
            os.mkdir(new_dir_path)
        except OSError as e:
            if e.errno != errno.EEXIST:
                #directory already exists
                pass
            
        vs = cv2.VideoCapture(camera.ip_address)
        path = settings.MEDIA_ROOT
        
        context['cap_image'] = list()
        

        folders = os.listdir(new_dir_path+"/")

        # for f in folders:
        context['cap_image']+= [os.path.join("/media"+"/"+request.POST.get('Part_name')+"/", file) for file in folders]
        while True:
            # grab the frame from the threaded video stream
            ret, frame = vs.read()

            image = cv2.imwrite(new_dir_path+"/"+str(random.randint(0, 500) )+".jpg", frame)
            
            return render(request, "getdata.html", context)    





def find_camera(slug):
    cameras = Cameras.objects.get(slug=slug)
    return cameras.ip_address
#  for cctv camera use rtsp://username:password@ip_address:554/user=username_password='password'_channel=channel_number_stream=0.sdp' instead of camera
#  for webcam use zero(0)
 

def gen_frames(camera_id):
     
    cam = find_camera(camera_id)
    cap=  cv2.VideoCapture(cam)
    
    while True:
        # for cap in caps:
        # # Capture frame-by-frame
        success, frame = cap.read()  # read the camera frame
        if not success:
            break
        else:
            ret, buffer = cv2.imencode('.jpg', frame)
            frame = buffer.tobytes()
            yield (b'--frame\r\n'
                b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')  # concat frame one by one and show result


def video_feed(request, slug):
   
    """Video streaming route. Put this in the src attribute of an img tag."""
    return StreamingHttpResponse(gen_frames(slug), content_type="multipart/x-mixed-replace;boundary=frame")
    # return StreamingHttpResponse(),
    #                 mimetype='multipart/x-mixed-replace; boundary=frame')


def detect(request):
    context = {}
    context['zones'] = Cameras.objects.all().order_by('id')
    context = {"success" : False}
    context['images'] = []
    context['processed_frame'] = []
    cameras = Cameras.objects.all().order_by('id')
    path= settings.MEDIA_ROOT
    for i, camera in enumerate(cameras):
        first_svm = camera_objects.objects.filter(camera__id = camera.id).first()
        detector= ObjectDetector(loadPath=path+"/"+str(first_svm.svms))

        vs = cv2.VideoCapture(camera.ip_address)
        path = settings.MEDIA_ROOT
        # while True:
        ret, frame = vs.read()
        string = str(random.randint(0, 500) )
        image = cv2.imwrite(path+"/Original_image/"+string+".jpg", frame)
        context['original_frame'+str(i)] = "/media/Original_image/"+string+".jpg"
        # data['original_frame'].append(data['original_frame'+str(i)])
        image = cv2.imread(path+"/Original_image/"+string+".jpg")
        print(image)
        print(camera.id)
        detected = detector.detect_zone2(image, camera.id, annotate="CENTER OK")
        img = Image.fromarray(detected, 'RGB')
        buffer = BytesIO()

        new_img = img.save(buffer,"PNG")
        # new_img.show()
        img_str = base64.b64encode(buffer.getvalue())
        context['image'+str(i)] = img_str.decode()
        # data['processed_frame'].append(data['image'+str(i)])
        context['images'].append({"Zone"+str(i):{"captured":context['original_frame'+str(i)],
                                              "processed":context['image'+str(i)]}})
        img1 = Image.fromarray(detected, 'RGB')
        processed_image = img1.save("media/Detected_image/"+string+".png")
        detectionlogs = detection()
        detectionlogs.detecting_object_name = camera.title
        detectionlogs.original_image = "/media/Original_image/"+string+".jpg"
        detectionlogs.detected_image = "/media/Detected_image/"+string+".png"
        detectionlogs.save()
    
    context['success'] = True
    return render(request, 'index.html', context)   



def point1(request):
    context = {}
    response=""
    svm_path = "SVMs/point1_right_multi.svm"
    cam_ip = "rtsp://admin:vijender1124@10.116.112.89:554/profile0" 
    point_num = "POINT 1 DETECTED"
    
    if request.session['point1'] == True:
        print("truiessssssssssssssss")
        point = requests.get('https://10.116.112.70:39320/iotgateway/read?ids=HONDANVH.TIPS.M3401', verify=False)
        objects = (json.loads(point.text))
        data = objects['readResults'][0]['v']

        if data == True:
            print("if case ")
            # pass
        else:
            print("else case ")
            response = detect_position_right(svm_path, cam_ip, point_num)

    else:   
        print('Faslseeeeeeeeeeeeeeeeeeeeeeeeeeeeeee') 
        point = requests.get('https://10.116.112.70:39320/iotgateway/read?ids=HONDANVH.TIPS.M796', verify=False)
        objects = (json.loads(point.text))
        data = objects['readResults'][0]['v']
        response = ""
        if data == True:
            
            path= settings.MEDIA_ROOT
            detector= ObjectDetector(loadPath=svm_path)
            vs = cv2.VideoCapture(cam_ip)
            ret, frame = vs.read()
            string = str(random.randint(0, 500) )
            image = cv2.imwrite(path+"/Original_image/"+string+".jpg", frame)
            image = cv2.imread(path+"/Original_image/"+string+".jpg")
            detected = detector.detect(image, annotate="POINT 1 DETECTED")
            print(detected)
            if detected == "POINT DETECTED":
                response = True
                request.session['point1'] = True
                
            else:
                response = False
                    
    return JsonResponse(response, safe=False)



def point2(request):
    context = {}

    response=""
    svm_path = "SVMs/point2_right_multi.svm"
    cam_ip = "rtsp://admin:vijender1124@10.116.112.89:554/profile0" 
    point_num = "POINT 2 DETECTED"
    
    if request.session['point2'] == True:
        print("truiessssssssssssssss")
        point = requests.get('https://10.116.112.70:39320/iotgateway/read?ids=HONDANVH.TIPS.M3402', verify=False)
        objects = (json.loads(point.text))
        data = objects['readResults'][0]['v']

        if data == True:
            print("if case ")
            # pass
        else:
            print("else case ")
            response = detect_position_right(svm_path, cam_ip, point_num)

    else:   
        print('Faslseeeeeeeeeeeeeeeeeeeeeeeeeeeeeee') 
        point = requests.get('https://10.116.112.70:39320/iotgateway/read?ids=HONDANVH.TIPS.M796', verify=False)
        objects = (json.loads(point.text))
        data = objects['readResults'][0]['v']
        response = ""
        if data == True:
            
            path= settings.MEDIA_ROOT
            detector= ObjectDetector(loadPath=svm_path)
            vs = cv2.VideoCapture(cam_ip)
            ret, frame = vs.read()
            string = str(random.randint(0, 500) )
            image = cv2.imwrite(path+"/Original_image/"+string+".jpg", frame)
            image = cv2.imread(path+"/Original_image/"+string+".jpg")
            detected = detector.detect(image, annotate="POINT 2 DETECTED")
            print(detected)
            if detected == "POINT DETECTED":
                response = True
                request.session['point2'] = True
                
            else:
                response = False
                    
    return JsonResponse(response, safe=False)


def point3(request):
    context = {}
    response=""
    svm_path = "SVMs/POINT3_right_multi.svm"
    cam_ip = "rtsp://admin:vijender1124@10.116.112.89:554/profile0" 
    point_num = "POINT 3 DETECTED"
    
    if request.session['point3'] == True:
        print("truiessssssssssssssss")
        point = requests.get('https://10.116.112.70:39320/iotgateway/read?ids=HONDANVH.TIPS.M3403', verify=False)
        objects = (json.loads(point.text))
        data = objects['readResults'][0]['v']

        if data == True:
            print("if case ")
            # pass
        else:
            print("else case ")
            response = detect_position_right(svm_path, cam_ip, point_num)

    else:   
        print('Faslseeeeeeeeeeeeeeeeeeeeeeeeeeeeeee') 
        point = requests.get('https://10.116.112.70:39320/iotgateway/read?ids=HONDANVH.TIPS.M796', verify=False)
        objects = (json.loads(point.text))
        data = objects['readResults'][0]['v']
        response = ""
        if data == True:
            
            path= settings.MEDIA_ROOT
            detector= ObjectDetector(loadPath=svm_path)
            vs = cv2.VideoCapture(cam_ip)
            ret, frame = vs.read()
            string = str(random.randint(0, 500) )
            image = cv2.imwrite(path+"/Original_image/"+string+".jpg", frame)
            image = cv2.imread(path+"/Original_image/"+string+".jpg")
            detected = detector.detect(image, annotate="POINT 3 DETECTED")
            print(detected)
            if detected == "POINT DETECTED":
                response = True
                request.session['point3'] = True
                
            else:
                response = False
                    
    return JsonResponse(response, safe=False)


def point4(request):
    
    context = {}
    point = requests.get('https://10.116.112.70:39320/iotgateway/read?ids=HONDANVH.TIPS.X049', verify=False)
    objects = (json.loads(point.text))
    data = objects['readResults'][0]['v']
    return JsonResponse(data, safe=False)

###### spare gun Right / NVH3 #####

def spare_rightpt1(request):
    context = {}
    response=""
    svm_path = "SVMs/point1_right_multi.svm"
    cam_ip = "rtsp://admin:taikisha@2019@10.116.112.94:554/profile0" 
    point_num = "POINT 1 DETECTED"
    
    if request.session['point1'] == True:
        print("truiessssssssssssssss")
        point = requests.get('https://10.116.112.70:39320/iotgateway/read?ids=HONDANVH.TIPS.M3421', verify=False)
        objects = (json.loads(point.text))
        data = objects['readResults'][0]['v']

        if data == True:
            print("if case ")
            # pass
        else:
            print("else case ")
            response = detect_position_rightsp(svm_path, cam_ip, point_num)

    else:   
        print('Faslseeeeeeeeeeeeeeeeeeeeeeeeeeeeeee') 
        point = requests.get('https://10.116.112.70:39320/iotgateway/read?ids=HONDANVH.TIPS.M798', verify=False)
        objects = (json.loads(point.text))
        data = objects['readResults'][0]['v']
        response = ""
        if data == True:
            
            path= settings.MEDIA_ROOT
            detector= ObjectDetector(loadPath=svm_path)
            vs = cv2.VideoCapture(cam_ip)
            ret, frame = vs.read()
            string = str(random.randint(0, 500) )
            image = cv2.imwrite(path+"/Original_image/"+string+".jpg", frame)
            image = cv2.imread(path+"/Original_image/"+string+".jpg")
            detected = detector.detect(image, annotate="POINT 1 DETECTED")
            print(detected)
            if detected == "POINT DETECTED":
                response = True
                request.session['point1'] = True
                
            else:
                response = False
                    
    return JsonResponse(response, safe=False)



def spare_rightpt2(request):
    context = {}

    response=""
    svm_path = "SVMs/point2_right_multi.svm"
    cam_ip = "rtsp://admin:taikisha@2019@10.116.112.94:554/profile0" 
    point_num = "POINT 2 DETECTED"
    
    if request.session['point2'] == True:
        print("truiessssssssssssssss")
        point = requests.get('https://10.116.112.70:39320/iotgateway/read?ids=HONDANVH.TIPS.M3422', verify=False)
        objects = (json.loads(point.text))
        data = objects['readResults'][0]['v']

        if data == True:
            print("if case ")
            # pass
        else:
            print("else case ")
            response = detect_position_rightsp(svm_path, cam_ip, point_num)

    else:   
        print('Faslseeeeeeeeeeeeeeeeeeeeeeeeeeeeeee') 
        point = requests.get('https://10.116.112.70:39320/iotgateway/read?ids=HONDANVH.TIPS.M798', verify=False)
        objects = (json.loads(point.text))
        data = objects['readResults'][0]['v']
        response = ""
        if data == True:
            
            path= settings.MEDIA_ROOT
            detector= ObjectDetector(loadPath=svm_path)
            vs = cv2.VideoCapture(cam_ip)
            ret, frame = vs.read()
            string = str(random.randint(0, 500) )
            image = cv2.imwrite(path+"/Original_image/"+string+".jpg", frame)
            image = cv2.imread(path+"/Original_image/"+string+".jpg")
            detected = detector.detect(image, annotate="POINT 2 DETECTED")
            print(detected)
            if detected == "POINT DETECTED":
                response = True
                request.session['point2'] = True
                
            else:
                response = False
                    
    return JsonResponse(response, safe=False)


def spare_rightpt3(request):
    context = {}
    response=""
    svm_path = "SVMs/POINT3_right_multi.svm"
    cam_ip = "rtsp://admin:taikisha@2019@10.116.112.94:554/profile0" 
    point_num = "POINT 3 DETECTED"
    
    if request.session['point3'] == True:
        print("truiessssssssssssssss")
        point = requests.get('https://10.116.112.70:39320/iotgateway/read?ids=HONDANVH.TIPS.M3423', verify=False)
        objects = (json.loads(point.text))
        data = objects['readResults'][0]['v']

        if data == True:
            print("if case ")
            # pass
        else:
            print("else case ")
            response = detect_position_rightsp(svm_path, cam_ip, point_num)

    else:   
        print('Faslseeeeeeeeeeeeeeeeeeeeeeeeeeeeeee') 
        point = requests.get('https://10.116.112.70:39320/iotgateway/read?ids=HONDANVH.TIPS.M798', verify=False)
        objects = (json.loads(point.text))
        data = objects['readResults'][0]['v']
        response = ""
        if data == True:
            
            path= settings.MEDIA_ROOT
            detector= ObjectDetector(loadPath=svm_path)
            vs = cv2.VideoCapture(cam_ip)
            ret, frame = vs.read()
            string = str(random.randint(0, 500) )
            image = cv2.imwrite(path+"/Original_image/"+string+".jpg", frame)
            image = cv2.imread(path+"/Original_image/"+string+".jpg")
            detected = detector.detect(image, annotate="POINT 3 DETECTED")
            print(detected)
            if detected == "POINT DETECTED":
                response = True
                request.session['point3'] = True
                
            else:
                response = False
                    
    return JsonResponse(response, safe=False)






###### spare gun Right / NVH3 #####


def sparep1(request):
    context = {}
    point = requests.get('https://10.116.112.70:39320/iotgateway/read?ids=HONDANVH.TIPS.M798', verify=False)
    objects = (json.loads(point.text))
    data = objects['readResults'][0]['v']
    # print("***************************************************************************",data)
    # camera = Cameras.objects.get(id=1)
    response = ""
    if data == True:
        path= settings.MEDIA_ROOT
        detector= ObjectDetector(loadPath="/home/i-tips/Desktop/i-tips_honda/SVMs/rightp1.1.svm")

        # vs = cv2.VideoCapture(camera.ip_address)
        
        vs = cv2.VideoCapture("rtsp://admin:taikisha@2019@10.116.112.94:554/profile0")
        path = settings.MEDIA_ROOT
        # while True:
        ret, frame = vs.read()
        string = str(random.randint(0, 500) )
        image = cv2.imwrite(path+"/Original_image/"+string+".jpg", frame)
        image = cv2.imread(path+"/Original_image/"+string+".jpg")
        detected = detector.detect(image, annotate="POINT 1 DETECTED")
        print(detected)
        if detected == "POINT DETECTED":
            response = True
            request.session['point1'] = True
        else:
            response = False    
    return JsonResponse(response, safe=False)



def sparep2(request):
    context = {}
    point = requests.get('https://10.116.112.70:39320/iotgateway/read?ids=HONDANVH.TIPS.M798', verify=False)
    objects = (json.loads(point.text))
    data = objects['readResults'][0]['v']
    # camera = Cameras.objects.get(id=1)
    response = ""
    if data == True:
        path= settings.MEDIA_ROOT
        detector= ObjectDetector(loadPath="/home/i-tips/Desktop/i-tips_honda/SVMs/p2right_1feb.svm")
        # vs = cv2.VideoCapture(camera.ip_address)
        
        vs = cv2.VideoCapture("rtsp://admin:taikisha@2019@10.116.112.94:554/profile0")
        
        path = settings.MEDIA_ROOT
        # while True:
        ret, frame = vs.read()
        string = str(random.randint(0, 500) )
        image = cv2.imwrite(path+"/Original_image/"+string+".jpg", frame)
        image = cv2.imread(path+"/Original_image/"+string+".jpg")
        detected = detector.detect(image, annotate="POINT 2 DETECTED")
        print(detected)
        
        if detected == "POINT DETECTED":
            response = True
            request.session['point2'] = True
        else:
            response = False    
    return JsonResponse(response, safe=False)
 


def sparep3(request):
    
    context = {}
    point = requests.get('https://10.116.112.70:39320/iotgateway/read?ids=HONDANVH.TIPS.M798', verify=False)
    objects = (json.loads(point.text))
    data = objects['readResults'][0]['v']
    # print(data)
    response = ""
    # camera = Cameras.objects.get(id=1)
    if data == True:
        path= settings.MEDIA_ROOT
        detector= ObjectDetector(loadPath="/home/i-tips/Desktop/i-tips_honda/SVMs/p3rightspare.svm")
        # vs = cv2.VideoCapture(camera.ip_address)
        
        vs = cv2.VideoCapture("rtsp://admin:taikisha@2019@10.116.112.94:554/profile0")
        
        path = settings.MEDIA_ROOT
        # while True:
        ret, frame = vs.read()
        string = str(random.randint(0, 500) )
        image = cv2.imwrite(path+"/Original_image/"+string+".jpg", frame)
        image = cv2.imread(path+"/Original_image/"+string+".jpg")
        detected = detector.detect(image, annotate="POINT 3 DETECTED")
        print(detected)
        
        if detected == "POINT DETECTED":
            response = True
            request.session['point3'] = True
        else:
            response = False    
    return JsonResponse(response, safe=False)

def kep(request):
    context = {}
    point = requests.get('https://10.116.112.70:39320/iotgateway/read?ids=HONDANVH.TIPS.D600', verify=False)
    objects = (json.loads(point.text))
    return JsonResponse(objects, safe=False) 

def kephumid(request):
    context = {}
    point = requests.get('https://10.116.112.70:39320/iotgateway/read?ids=HONDANVH.TIPS.D610', verify=False)
    objects = (json.loads(point.text))
    return JsonResponse(objects, safe=False)

def asuchamber(request):
    context = {}
    point = requests.get('https://10.116.112.70:39320/iotgateway/read?ids=HONDANVH.TIPS.D620', verify=False)
    objects = (json.loads(point.text))
    return JsonResponse(objects, safe=False)

def ambtemp(request):
    context = {}
    point = requests.get('https://10.116.112.70:39320/iotgateway/read?ids=HONDANVH.TIPS.D630', verify=False)
    objects = (json.loads(point.text))
    return JsonResponse(objects, safe=False)

def body_count(request):
    context = {}
    point = requests.get('https://10.116.112.70:39320/iotgateway/read?ids=HONDANVH.TIPS.D3420', verify=False)
    objects = (json.loads(point.text))
    return JsonResponse(objects, safe=False)    

def airvelocity(request):
    context = {}
    point = requests.get('https://10.116.112.70:39320/iotgateway/read?ids=HONDANVH.TIPS.D3392', verify=False)
    objects = (json.loads(point.text))
    return JsonResponse(objects, safe=False)



# def model_check(request):
    
#     context = {}
#     point = requests.get('https://10.116.112.70:39320/iotgateway/read?ids=HONDANVH.TIPS.M890', verify=False)
#     objects = (json.loads(point.text))
#     data = objects['readResults'][0]['v']
#     return JsonResponse(data, safe=False)    
