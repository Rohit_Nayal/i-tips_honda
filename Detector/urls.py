from django.urls import include, path, re_path
from Detector import views

urlpatterns = [
    path('', views.index, name='home'),
    path('point1/', views.point1, name='point1'),
    path('point2/', views.point2, name='point2'),
    path('point3/', views.point3, name='point3'),
    path('point4/', views.point4, name='point4'),

    path('Lpoint1/', views.Lpoint1, name='Lpoint1'),
    path('Lpoint2/', views.Lpoint2, name='Lpoint2'),
    path('Lpoint3/', views.Lpoint3, name='Lpoint3'),
    path('Lpoint4/', views.Lpoint4, name='Lpoint4'),

    path('spare_rightpt1/', views.spare_rightpt1, name='spare_rightpt1'),
    path('spare_rightpt2/', views.spare_rightpt2, name='spare_rightpt2'),
    path('spare_rightpt3/', views.spare_rightpt3, name='spare_rightpt3'),

    path('sparep1/', views.sparep1, name='sparep1'),
    path('sparep2/', views.sparep2, name='sparep2'),
    path('sparep3/', views.sparep3, name='sparep3'),
    

    path('kep/', views.kep, name='kep'),
    path('kephumid/', views.kephumid, name='kephumid'),
    path('asuchamber/', views.asuchamber, name='asuchamber'),
    path('ambtemp/', views.ambtemp, name='ambtemp'),
    path('airvelocity/', views.airvelocity, name='airvelocity'),
    path('production_count/', views.body_count, name="production_count"),
    
    # path('model_check/', views.model_check, name='model_check'),
    path('annotate/', views.gather_annotation, name='annotate'),
    path('train/', views.train, name='train'),
    path('getdata/', views.get_cam, name='get_dataset'),
    path('rightside/', views.rightside, name='rightside'),
    path('leftside/', views.leftside, name='leftside'),
    path('spare/', views.spare, name='spare'),
    re_path(r'getdata/(?P<slug>[\w\-]+)/$', views.video_feed, name='new_webcam'),
    re_path(r'capture/(?P<slug>[\w\-]+)/$', views.capture, name='capture'),
    path('object_detection/detect/', views.detect, name = "detect_objects"),
   
]