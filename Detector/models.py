from django.db import models
from django.template.defaultfilters import slugify
from django.contrib.auth.models import User
# Create your models here.
def get_upload_svm_path(instance, filename):
        return '{0}/{1}'.format(instance.camera.title+ "/svm/", filename)

def get_upload_image_path(instance, filename):
        return '{0}/{1}'.format(instance.camera.title+ "/images/", filename)

def get_upload_annot_path(instance, filename):
        return '{0}/{1}'.format(instance.camera.title + "/annotation/", filename)  

class Cameras(models.Model):
    title = models.CharField(max_length=255)
    slug = models.SlugField(unique=True, max_length=255, null=True, blank=True)
    ip_address = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)
    
    def save(self, *args, **kwargs):
        if not self.slug:
         self.slug = slugify(self.title)
         super(Cameras, self).save(*args, **kwargs)
    
    def __str__(self):
        return self.title     


class camera_objects(models.Model):
    camera = models.ForeignKey(Cameras, on_delete=models.CASCADE, related_name='scan_objects')
    svms = models.FileField(upload_to=get_upload_svm_path) 


class Annotations(models.Model):
    camera_name = models.ForeignKey(Cameras, on_delete=models.CASCADE, related_name='zone_annotations')
    image_matrix = models.FileField(upload_to=get_upload_image_path)
    annotation_matrix = models.FileField(upload_to=get_upload_annot_path)

   

class detection(models.Model):
    detecting_object_name = models.CharField(max_length=500)
    original_image = models.ImageField(upload_to="original_image/", height_field=None, width_field=None)
    detected_image = models.ImageField(upload_to="detected_image/", height_field=None, width_field=None)
    created_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.detecting_object_name
