import sys, os, cv2
from django.conf import settings
import numpy as np
from ROI.box_selector import BoxSelector


def create_annotation(annotaion, img_annotation, dataset):
    if annotaion.endswith('.npy'):
        annotaion = annotaion
    else:
        annotaion =  annotaion +".npy"  

    if img_annotation.endswith('.npy'):
        img_annotation = img_annotation
    else:
        img_annotation =  img_annotation +".npy"       
    #parse arguments
    # ap = argparse.ArgumentParser()
    # ap.add_argument("-d","--dataset",required=True,help="path to images dataset...")
    # ap.add_argument("-a","--annotations",required=True,help="path to save annotations...")
    # ap.add_argument("-i","--images",required=True,help="path to save images")
    # args = vars(ap.parse_args())

    # #annotations and image paths
    annotations = []
    imPaths = []
    path = settings.MEDIA_ROOT
    folders = os.listdir(path+"/"+dataset+"/")
    # list_images = [os.path.join("/media/Cam1/", file) for file in folders]
    #loop through each image and collect annotations
    for imagePath in folders:
        # imagePath = "/media/Cam1/"+imagePath
        fullPath = os.path.join(path+"/"+dataset+"/", imagePath)
        print(fullPath)
        if not imagePath.startswith('.'):
        # If entry is a directory then get the list of files in this directory 
        # if os.path.isdir(fullPath):
            #load image and create a BoxSelector instance
            image = cv2.imread(fullPath)
            bs = BoxSelector(image,"Image")
            cv2.imshow("Image",image)
            # img = Image.fromarray(image, 'RGB')
            # buffer = BytesIO()

            # new_img = img.save(buffer,"PNG")
            # img_str = base64.b64encode(buffer.getvalue())
            # context['image'] = img_str.decode()
            cv2.waitKey(0)

            #order the points suitable for the Object detector
            pt1,pt2 = bs.roiPts
            (x,y,xb,yb) = [pt1[0],pt1[1],pt2[0],pt2[1]]
            annotations.append([int(x),int(y),int(xb),int(yb)])
            imPaths.append(fullPath)

    #save annotations and image paths to disk
    annotations = np.array(annotations)
    imPaths = np.array(imPaths,dtype="unicode")
    annot = np.save("Annotations/"+annotaion,annotations)
    image_matrix = np.save("Image_matrix/"+img_annotation,imPaths)
    print(image_matrix)
    print(annot)
    cv2.destroyAllWindows()
    val="Annotation completed now train the system" 
    return val

if __name__ == '__main__':
    try:
        arg = sys.argv[1]
    except IndexError:
        arg = None

    return_val = create_annotation(arg)

